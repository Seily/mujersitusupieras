<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mujersitusupieras' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );



// WEBHOST
// define( 'DB_NAME', 'id14538608_msts' );
// define( 'DB_USER', 'id14538608_mstsguatemala' );
// define( 'DB_PASSWORD', ')grq%1gooB4^w&b#2!R&' );
// define( 'DB_HOST', 'localhost' );
// define( 'DB_CHARSET', 'utf8mb4' );
// define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'nEg_Fftq&9PPM;3+)5Bs4V_qgOTcJ!ygfKQ_K!x#f7#X`8Czo;]h3MN5S}q)@uN.' );
define( 'SECURE_AUTH_KEY',  'M}LDQ[dPow*)0~b[:AJtd5sXm>d/(!pPU7g[L[psy|%lV?^DwU|8_4?:U1Zkq@<u' );
define( 'LOGGED_IN_KEY',    'y/7$Na]dw80:7e|>#KhiZTt4Z)}4If[9#%b!}#LscwQOug9}?YZ)`5S~S4R:@PEv' );
define( 'NONCE_KEY',        '~#T.]!?:YOk~1q[RE<5)2z`#blG[.L>7/PGV!D2Iyt:[@G}~h(HH?}q@qxbUPrap' );
define( 'AUTH_SALT',        'Ft%jI(XSfjB.xs]a[@X3i1va%Uk!lt,9WA7qwjK,pY7W.8Y=Lwgzu.)jo;9e-&SV' );
define( 'SECURE_AUTH_SALT', 'NLi?iF2#tbL*_jda}z8,4{_?G#o*iMU?lQC=Az|*iAh93Qy N1nqbVOb=-AO#k|?' );
define( 'LOGGED_IN_SALT',   'rf~Edqa-0ZH K`yND^6L/%tf)fyJh+U0yPyZ-3Q8:GI;a 8kT|FI!Pz4P7[J7EX|' );
define( 'NONCE_SALT',       'TlPzEegvP7VT<{KNqz_drZlu.3ycS^=8{J,?#9K,E1d`:~P34&R-f-XXJ|WI{jQn' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'msts_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
