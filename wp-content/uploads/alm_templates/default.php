<div class="category-recurso">
<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('category-thumb');?></a>
<h3 class="category-title">
<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
<b> <?php the_title(); ?></b>
</a>
</h3>
<?php the_excerpt(); ?>
</div>