<?php
/**
 * Template Name: Home Page
 *
 * Displays the Home page with Parallax effects.
 *
 */
?>

<?php get_header(); ?>
<?php
echo do_shortcode('[smartslider3 slider="2"]'); 
// echo do_shortcode('[slide-anything id="1450"]'); 
?>

<div id="shapely_home_parallax-3" class="widget shapely_home_parallax">
	<section class="">
		<div class="container-fluid" id="home-video">
			<div class="row align-children">

				<div class="col-md-6 col-sm-6 text-center mb-xs-24">
					<img class="img-responsive" alt="contenido video" style="margin: 0;"
						src="/wp-content/uploads/2020/06/2020_contenidovideo.png">
				</div>

				<div class="col-md-3 col-sm-5 contenidovideo-home">
					<div class="">
						<h3>Contenido en video</h3>
						<div class="mb32">
							<p>Únete a nuestra comunidad en Youtube, suscribiéndote gratuitamente.
								Al suscribirte recibirás notificaciones cuando estemos transmitiendo nuevo contenido
								para ti.
							</p>
						</div><a class="btn btn-lg" href="/videos/">Mira aquí</a>
					</div>
				</div>
				<!--end of row-->
			</div>
		</div>
	</section>
	<div class="clearfix"></div>
</div>


<div class="container-fluid" id="home-audio">

	<div class="row align-children">


		<div class="col-md-3 col-sm-5 mb-xs-24">
		<div class="col-md-6  col-sm-6 mobile">
			<img class="img-responsive float-right" alt="contenido audio" style="margin:0 auto; float:right"
				src="/wp-content/uploads/2020/06/2020_contenidoaudio.png">
		</div>
			<div class="text-right">
				<h3>Contenido en audio</h3>
				<div class="mb32">
					<p>Únete a nuestra comunidad en Youtube, suscribiéndote gratuitamente.
						Al suscribirte recibirás notificaciones cuando estemos transmitiendo nuevo contenido para ti.
					</p>
				</div>
				<a class="btn btn-lg" href="/podcast/">Escúcha aquí</a>
			</div>
		</div>
		<!--end of row-->
		<div class="col-md-6  col-sm-6 desktop">
			<img class="img-responsive float-right" alt="contenido audio" style="margin:0 auto; float:right"
				src="/wp-content/uploads/2020/06/2020_contenidoaudio.png">
		</div>
	</div>
</div>


<div id="" class="reflexioneshome">
				<img src="/wp-content/uploads/2020/07/2020_bannerhome_reflexiones.jpg" class="desktop">
				<img src="/wp-content/uploads/2020/08/2020_bannerhome_reflexiones_mobile.jpg" class="mobile">
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center contenidorefhome">
							<h1 style="color:#ffffff">Reflexiones</h1>
							<a class="btn btn-lg btn-filled" href="/category/reflexiones/">Leer más</a>
						</div>
					<!--end of row-->
	<div class="clearfix"></div>
</div>



<?php //if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-home' ) ) :?>
<!--<div class="container p24 wp-caption-text">
		<h5><?php // echo esc_html__( 'This is the "Home Sidebar Section", add some widgets to it to change it.', 'shapely' ); ?></h5>
	</<div>-->
<?php // endif; ?>


<?php get_footer(); ?>
