<?php
/*
Template Name: Videos
Template Post Type: post, page
*/
get_header(); ?>
</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<br><br>
<div id="main" class="container" role="main">

<div id="primary" class="col-md-12 mb-xs-24">
			<?php
            while ( have_posts() ) :
                the_post();
				the_content();

			endwhile; // End of the loop.
			?>
		</div><!-- #primary -->
    
    
<?php get_footer(); ?>