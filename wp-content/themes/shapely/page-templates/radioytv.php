<?php
/*
Template Name: radioytv
Template Post Type: post, page
*/
get_header(); ?>
</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<br><br>

<center>
<div id="main" class="container" role="main">
<div class="radiotv">
<h2>Televisión</h2>
<p>Sintoniza “Mujer, ¡Si tú supieras!”  todos los <b>Lunes a las 09:00 P.M.</b> y <b>Miércoles a las 11:00 A.M.</b> en</p>
<img src="<?php echo get_template_directory_uri()?>/assets/images/logo_television_arquidiocesana.png" alt="">
<br><br><br><br>
<p>Sintoniza “Mujer, ¡Si tú supieras!”  todos los <b>Sábados a las 12:30 P.M. </b>en</p>
<img src="<?php echo get_template_directory_uri()?>/assets/images/logo_esne.png" alt="">
</div>

<div class="divider"></div>

<div class="radiotv">
<h2>Radio</h2>
<p>Sintoniza “Mujer, ¡Si tú supieras!”  todos los <b>Martes a las 10:15 A.M. </b>en</p>
<img src="<?php echo get_template_directory_uri()?>/assets/images/logo_radio_maria.png" alt="">
</div>
</center>
    
<?php get_footer(); ?>