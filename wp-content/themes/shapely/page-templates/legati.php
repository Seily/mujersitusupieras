<?php
/*
Template Name: legati
Template Post Type: post, page
*/
get_header(); ?>

</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?><br><p style=" font-family: 'neutra-text-bold-italic'; font-size: 20px">Detalles con palabra de Dios</p></h1>

<?php endif; ?>
<div id="main" class="" role="main">
<center>
	<div class="row container">
<p style="font-size: 24px !important;">  <?php echo get_field('introduccion');?></p>
  <h5 class="contactanossubtitle">Agendas Mujer, si tú supieras</h5>
  <?php echo get_field('agendas');?>
  <h5 class="contactanossubtitle">Llámanos o escríbenos</h5>
  <?php if(have_rows('contactanos')): ?>
            <?php while( have_rows('contactanos') ): the_row(); ?>
              <a href="<?php the_sub_field('contacto'); ?>">
                <p style="    font-size: 25px;"> <img src="<?php echo get_template_directory_uri()?>/assets/images/callus_icon.png" alt="" style="margin-right:20px"> <?php echo the_sub_field('nombre_canal_de_contacto'); ?> </p>
              </a>
            <?php endwhile; ?>
          <?php endif; ?>
          <div class="col-lg-12 redescontactus">
          <h5 class="contactanossubtitle">Síguenos en redes sociales</h5>
          <?php if(have_rows('siguenos_en_redes')): ?>
            <?php while( have_rows('siguenos_en_redes') ): the_row(); ?>
              <a href="<?php the_sub_field('link'); ?>">
              <?php	$image = get_sub_field('logo');?>
                <img src="<?php echo $image; ?>" alt="">
              </a>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
  </div>
  </center><br style="display:block"><br style="display:block"><br style="display:block">
<?php get_footer(); ?>