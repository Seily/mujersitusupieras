<?php
/*
Template Name: Explora
Template Post Type: post, page
*/
get_header(); ?>
<?php
global $wp_query;
?>
</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<br><br>
<div id="main" class="container" role="main">
<div class="col-lg-8" style="border-right: solid #C6A155 2px;     margin-bottom: 50px; min-height:500px">

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type'=> 'post',
    'orderby'    => 'date',
    'post_status' => 'publish',
    'order'    => 'DESC',
    'posts_per_page' => 18 // this will retrive all the post that is published 
    );
    $result = new WP_Query( $args );
    if ( $result-> have_posts() ) : ?>
    <div class="tagged-posts">
    <h4 class="text-center">Recientes</h4><br><br>
    <?php while ( $result->have_posts() ) : $result->the_post(); ?>
    
	<div class="posts-explora ">
                    <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('category-thumb');?></a>
                    <!-- <div class="catpostex ">Categoría: <?php// the_category(); ?></div> -->
					<h3 class="category-title">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
							<b> <?php the_title(); ?></b>
						</a>
                    </h3>
                    
                    <?php the_excerpt(); ?>
                    
                </div>

    <?php endwhile; ?>
    </div>
    <?php endif; wp_reset_postdata(); ?>
</div>
<div class="col-lg-4">
<h4 class="text-center">Etiquetas</h4><br><br>
<?php tags_filter();?>
</div>
<?php get_footer(); ?>