<?php
/*
Template Name: conocenos
Template Post Type: post, page
*/
get_header(); ?>

</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<div id="main" class="" role="main">
<center>
	<div class="row container">
    
        
        <div class="col-lg-12">
          <h5>Nuestra Historia</h5>
          <p><?php the_field('nuestra_historia');?></p>
          <br><br>
          <h5>Misión</h5>
          <p><?php the_field('mision');?></p>
          <br><br>
          <h5>Visión</h5>
          <p><?php the_field('vision');?></p>
          <br><br>
          <h5>Área de trabajo</h5>
          <p><?php the_field('area_de_trabajo');?></p>
        </div>
        
        <?php if(have_rows('nuestro_equipo')): ?>
          
          <h5 class="title-equipo">Nuestro Equipo</h5>
        <div class="col-lg-12">
        <?php $i = 0; ?>
        <?php while( have_rows('nuestro_equipo') ): the_row(); ?>
          <aside id="servidoras_<?php echo ++$i;?>" class="servidora-container">
          <?php	$image = get_sub_field('fotografia');	if( !empty($image) ): ?>
                  <img src="<?php echo $image; ?>" />
                <?php endif; ?>
              <p><?php the_sub_field('nombre'); ?></p>
              <p><?php the_sub_field('descripcion'); ?></p>

          </aside>
        <?php endwhile; ?>
        

        </div>
        <?php endif; ?>
        
  </div>
  </center>
<?php get_footer(); ?>