<?php
/*
Template Name: Sedes
Template Post Type: post, page
*/
get_header(); ?>

</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<div id="main" class="" role="main">



<!-- <div class="noticias-search-input ">
			<select name="yr" id="yr" onchange="this.form.submit()">
				<option selected="selected" value="" disabled="disabled">Elige</option>
        <option value="seminario">Seminario</option>
        <option value="desayuno">Desayuno</option>
        <option value="retiro">Retiro</option>
        <option value="curso">Curso</option>
				</select>
  </div> -->


	<div class="row">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if(have_rows('sede')): ?>
    <?php $i = 0; ?>
    <?php while( have_rows('sede') ): the_row(); ?>
    
      <section id="sede_<?php echo ++$i;?>" class="sede-container">
        <ul class="sede-varios">
          
          <li><b><?php the_sub_field('nombre_sede'); ?></b>
          </li>
          <li>
            <b><?php the_sub_field('direccion_sede'); ?></b>
          </li>
          <li>
            <?php	$image = get_sub_field('foto_sede');	if( !empty($image) ): ?>
              <div style="background-image: url(<?php echo $image; ?>)">
              <img src="<?php echo get_bloginfo('template_url') ?>/assets/images/frame-actividades.png" />
              </div>
            <?php endif; ?>
          </li>
        </ul>
          <div class="divider"></div>
        </section>
      <?php endwhile; ?>
      <?php endif; ?>
        </article>
	</div>
<?php get_footer(); ?>