<?php
/*
Template Name: contactanos
Template Post Type: post, page
*/
get_header(); ?>

</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<div id="main" class="" role="main">
<center>
	<div class="row container">
    
  <div class="col-lg-12"><br><br><br>
    <h2>Apostolado Católico</h2>
    <img src="<?php echo get_template_directory_uri()?>/assets/images/logomsts_square.png" class="logocontactus">
  </div>
        <!-- <div class="col-lg-12">
          <h5 class="contactanossubtitle"><img src="<?php // echo get_template_directory_uri()?>/assets/images/visitus_icon.png" alt="">Visítanos</h5>
          <p><?php // the_field('visitanos');?></p>
        </div> -->
        
        <div class="col-lg-12 llamanos">
          <h5 class="contactanossubtitle"><img src="<?php echo get_template_directory_uri()?>/assets/images/callus_icon.png" alt="">Llámanos o escríbenos</h5>
          <?php if(have_rows('telefono_informacion')): ?>
            <div class="callustitle">Información:</div>
            <?php while( have_rows('telefono_informacion') ): the_row(); ?>
              <div><?php the_sub_field('numero'); ?></div>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php if(have_rows('telefono_oracion_e_intecesion')): ?>
            <div class="callusdivider"></div>
            <div class="callustitle">Oración e intercesión:</div>
            <?php while( have_rows('telefono_oracion_e_intecesion') ): the_row(); ?>
              <div><?php the_sub_field('numero'); ?></div>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php if(have_rows('telefono_legati_fidei')): ?>
            <div class="callusdivider"></div>
            <div class="callustitle">Legati Fidei:</div>
            <?php while( have_rows('telefono_legati_fidei') ): the_row(); ?>
              <div><?php the_sub_field('numero'); ?></div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div class="col-lg-12 redescontactus">
          <h5 class="contactanossubtitle">Síguenos en redes sociales</h5>
          <?php if(have_rows('siguenos_en_redes')): ?>
            <?php while( have_rows('siguenos_en_redes') ): the_row(); ?>
              <a href="<?php the_sub_field('link'); ?>">
              <?php	$image = get_sub_field('logo');?>
                <img src="<?php echo $image; ?>" alt="">
              </a>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div class="col-lg-12">
          <h5 class="contactanossubtitle">Escríbenos</h5>
          <div class="formulario-pregunta contactanos">
          <div class="form-frame contactanos"></div>
				<?php echo do_shortcode('[contact-form-7 id="1905" title="Contáctanos"]')?>
				
        </div>

        
  </div>
  </center>
<?php get_footer(); ?>