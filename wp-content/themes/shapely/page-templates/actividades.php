<?php
/*
Template Name: Actividades
Template Post Type: post, page
*/
get_header(); ?>

</div>
<?php	$image = get_field('escritorio');	if( !empty($image) ): ?>
<h1 class="innerpage-title" style="background-image: url(<?php echo $image; ?>)"> <?php echo the_title()?></h1>
<?php endif; ?>
<div id="main" class="" role="main">



<!-- <div class="noticias-search-input ">
			<select name="yr" id="yr" onchange="this.form.submit()">
				<option selected="selected" value="" disabled="disabled">Elige</option>
        <option value="seminario">Seminario</option>
        <option value="desayuno">Desayuno</option>
        <option value="retiro">Retiro</option>
        <option value="curso">Curso</option>
				</select>
  </div> -->


	<div class="row">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if(have_rows('actividades')): ?>
    <?php $i = 0; ?>
    <?php while( have_rows('actividades') ): the_row(); ?>
    
      <section id="actividad_<?php echo ++$i;?>" class="actividad-container">
        
          <aside>
            <?php	$image = get_sub_field('imagen');	if( !empty($image) ): ?>
              <div style="background-image: url()">
              <img src="<?php echo $image['url']; ?>" />
              </div>
            <?php endif; ?>
          </aside>
          <ul class="actividades-varios">
          <li><b><?php the_sub_field('tipo'); ?></b>
          </li>
          <li>
            <b><?php the_sub_field('titulo'); ?></b>
          </li>
          <li class="descripcion-actividad"><?php the_sub_field('descripcion'); ?>
          </li>
          <li><b class="ofrenda">Ofrenda: </b><?php the_sub_field('ofrenda'); ?>
          </li>
        </ul>
        <?php if( have_rows('informacion_de_la_actividad') ): ?>
          <table class="tabla-actividades">
            <tr>
              <td><b>Sede</b></td>
              <td><b>Fecha</b></td>
              <td><b>Hora</b></td>
            </tr>
            <?php while( have_rows('informacion_de_la_actividad') ): the_row(); ?>
              <tr>
                <td>
                  <?php the_sub_field('sede'); ?></td>
                  <?php if(get_sub_field('fecha')):?>
                  <td><?php the_sub_field('fecha'); ?></td>
                <?php endif;?>
                  <?php if(get_sub_field('rango_de_dias')):?>
                    <td><?php the_sub_field('rango_de_dias'); ?></td>
                    <?php endif;?>
                  <td><?php the_sub_field('hora'); ?></td>
                </tr>
              <?php endwhile; ?>
            </table>
          <?php endif; ?>
          <div class="espacio-actividad"></div>
        </section>
      <?php endwhile; ?>
    <?php else: ?>
      <h2 style="color:#C6A155; font-size:38px; margin: 25px 0 25px 0; text-align:center; font-family:neutra-text-book-alt !important " class="noHayActividades">Pronto tendremos más actividades para ti</h2>
    <?php endif; ?>
        </article>
	</div>
<?php get_footer(); ?>