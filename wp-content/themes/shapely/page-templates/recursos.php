<?php
/*
Template Name: Recursos
Template Post Type: post, page
*/
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<br><br>
<h1 class="recursos-title text-center"><?php echo the_title()?></h1>
</div>
<?php the_post_thumbnail();?>
<div class="container recursos-content">

<?php the_content(); ?>

<?php
$posttags = get_the_tags();
$count=0; $sep='';
if ($posttags) {
  echo "<div class='etiquetas'>";
	foreach($posttags as $tag) {
		$count++;
		echo $sep . '<a href="'.get_tag_link($tag->term_id).'" class="tags"><i class="fa fa-tag"></i> '.$tag->name.'</a>';
$sep = ' ';
		if( $count > 5 ) break; //change the number to adjust the count
  }
  echo "</div>";
}
?>
<div class="divider"></div>
<?php endwhile; ?>
<?php endif; ?>


<div style="float:left" class="recurso-relacionado"><?php previous_post_link('&lt; %link', "Anterior", true);?></div>
<div style="float:right" class="recurso-relacionado"><?php next_post_link('%link &gt;', "Siguiente", true);?></div>



</div>
<?php get_footer(); ?>