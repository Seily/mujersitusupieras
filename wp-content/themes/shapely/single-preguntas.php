<?php
/**
 * The template for displaying all single posts.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Shapely
 */

get_header(); ?>

	<div class="row"><br><br><br>
		<center><h2><?php  the_title()?></h2></center>
          <blockquote class="pregunta"><?php  the_field('pregunta')?></blockquote>
          <div class="respuesta"><h5 style="color: #f89686 !important">Respuesta:</h5><?php  the_field('respuesta')?></div>
		      
        <div class="divider"></div>
        <p class="etiquetas-pregunta"><?php the_tags();?></p>
        <br><br><br><br><br><br><br><br><br><br>
	</div>
<?php
get_footer();?>
