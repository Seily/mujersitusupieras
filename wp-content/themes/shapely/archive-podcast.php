<?php
/**
 * Archive podcast.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */
get_header(); ?>


</div>
	<h1 class="innerpage-title"><?php post_type_archive_title();?></h1>
		<div class="row" >
			<div class="container">
            <?php
				if ( have_posts() ) : ?>
					<?php
					// The Loop
					while ( have_posts() ) : the_post(); ?>
						<div class="podcast-episode">
							<?php
							the_post_thumbnail();
							the_content();
						?>
						</div>
					<?php endwhile;  
				endif; ?>

<?php shapely_pagination();?>
            </div>	
		</div>
<?php
get_footer();?>
