jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error

	$('.loadmore').click(function(){
		
		var button = $(this),
		    data = {
			'action': 'loadmore',
			'query': loadmore_params.posts, // that's how we get params from wp_localize_script() function
			'page' : loadmore_params.current_page
		};
		
	

		$.ajax({ // you can also use $.post here
			url : loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				$('.loading_ajax').show();
				button.text('cargando...'); // change the button text, you can also add a preloader image
			},
			
			success : function( data ){
				if( data ) { 
					$('.loading_ajax').hide();
					button.text( 'Ver más' ).prev().after(data); // insert new posts
					loadmore_params.current_page++;
					
					if ( loadmore_params.current_page == loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button
				} else {
					button.remove(); // if no data, remove the button as well
					$('.loading_ajax').hide();
				}
			}
		});


	});


	$('document').ready(function(){
		$('.darkc').hide();
		$('.formulario-pregunta').hide();
		$('.formulario-pregunta.contactanos').show();
		$('.wpcf7-submit').addClass('btn-filled btn btn-lg')
	})


	$('.preguntar-btn').click(function(){
		$('.darkc').show();
		$('.formulario-pregunta').show();
	})

	$('.darkc').click(function(){
		$('.darkc').hide();
		$('.formulario-pregunta').hide();
	})

	$('.darkc').click(function(){
		if($(".nf-response-msg").length){
			location.reload(true);
		}
	})


});

