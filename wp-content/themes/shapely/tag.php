<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

get_header(); ?>
</div><br><br>
<h1>Tag: <?php single_tag_title();?></h1>
		<div class="row" >
			<div class="container tags">
            <?php
				if ( have_posts() ) : ?>
					<?php
					// The Loop
					while ( have_posts() ) : the_post(); ?>
						<div class="podcast-episode">
							<?php
							the_post_thumbnail();?>
							<h5><?php the_title();?></h5>
							<?php the_excerpt();?>
						</div>
					<?php endwhile;  
				endif; ?>

<?php shapely_pagination();?>
            </div>	
		</div>
<?php
get_footer();
