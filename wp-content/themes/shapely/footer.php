<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?>

</div><!-- #main -->
</section><!-- section -->

<div class="footer-callout">
	<?php shapely_footer_callout(); ?>
</div>

<footer id="colophon" class="site-footer footer main-footer" role="contentinfo">
	<div class="container-fluid footer-inner">
		<div class="row">
			<?php get_sidebar( 'footer' ); ?>
		</div>

		<div class="row">
			<div class="site-info col-sm-4">
				<center>
				<div class="copyright-text">
					<img src="<?php echo get_bloginfo('template_url') ?>/assets/images/logoFooter.png"
						style="width: 300px; margin-top: -15px;">
				</div>
				</center>
			</div>
			<div class="col-sm-4">
				<center>
					<!--Suscribete-->
				<div class="tnp tnp-subscription">
					<h5>Suscríbete</h5>
					<form method="post" action="http://mujersitusupieras.local/?na=s"
						onsubmit="return newsletter_check(this)">
						<input type="hidden" name="nlang" value="">
						<span>Lo más importante en tu buzón electrónico cada mes, de forma gratuita</span>
						<div class="tnp-field tnp-field-email"><input class="tnp-email" type="email" name="ne"
								placeholder="Ingresa tu correo electrónico" required></div>
						<div class="tnp-field tnp-field-button "><input class="tnp-submit btn btn-filled" type="submit"
								value="Inscribirme">
						</div>
					</form>
				</div>
				</center>
			</div>
			<div class="col-sm-4 redes-sociales">
				<center>
				<h5>Redes sociales</h5>
				<ul class="">
					<li><a href="https://www.facebook.com/mujersitusupieras.gt/"><img src="<?php echo get_bloginfo('template_url') ?>/assets/images/msts_logofacebook.png" alt=""></a></li>
					<li><a href="https://www.instagram.com/mujersitusupieras/"><img src="<?php echo get_bloginfo('template_url') ?>/assets/images/msts_logoinstagram.png" alt=""></a></li>
					<li><a href="https://www.youtube.com/channel/UCXxCQrtC1j1fob6JOnCCdzQ"><img src="<?php echo get_bloginfo('template_url') ?>/assets/images/msts_logoyoutube.png" alt=""></a></li>
					<li><a href="https://open.spotify.com/show/3ep7PLrpbEaSdwtVkn9MNF?si=pWhlU9WTSA2BdDf2OW51gg"><img src="<?php echo get_bloginfo('template_url') ?>/assets/images/msts_logospotify.png" alt=""></a></li>
					<li><a href="https://api.whatsapp.com/send?phone=50249125656"><img src="<?php echo get_bloginfo('template_url') ?>/assets/images/msts_logowhatsapp.png" alt=""></a></li>
				</ul>
				</center>
			</div>
		</div>

	

	<a class="btn btn-sm fade-half back-to-top inner-link " href="#top"><i class="fa fa-angle-up"></i></a>
</footer><!-- #colophon -->
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
