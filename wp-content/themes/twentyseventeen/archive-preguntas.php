<?php
/**
 * The template for displaying archive pregunta.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */
get_header(); ?>

<?php $layout_class = shapely_get_layout_class(); ?>
</div>
	<h1 class="innerpage-title"><?php post_type_archive_title();?></h1>
		<div class="row" >
			<div class="container">
				<center>
					<div class="col-lg-6">
						<h5>Haz una pregunta</h5>
						<a href="#" class="preguntar-btn btn btn-filled" id="preguntar-btn">Preguntar</a>
					</div>
				</center>
				<center>
					<div class="col-lg-6">
						<h5>Encuentra respuestas</h5>
						<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
					</div>
				</center>
			</div>
			<div id="content" class="container preguntas">
				<?php
				if ( have_posts() ) : ?>
					<?php
					// The Loop
					while ( have_posts() ) : the_post(); ?>
					
							<div class="">
								<h3 class="pregunta-title"><?php the_title(); ?></h3>
								<?php the_field('pregunta'); ?><br>
								<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
								Lee la respuesta >
								</a>
							</div>
						<div class="divider"></div>
					<?php endwhile;  
				endif; ?>
			</div>
		</div>
		<?php include_once "popuppreguntar.php"?>
<?php
get_footer();?>
