<?php
/**
 * Hogar Archive pages.
 *
 * @package vogue
 * @since 1.0.0
 */

get_header();?>

<?php
global $wp_query;
?>

<main id="site-content" role="main" class="">
	<article class="row justify-content-center">
		<?php if (is_category('')) ?>
		</div>

		<h1 class="innerpage-title">
			<?php single_cat_title();?>
		</h1>
		<section id="primary" class="site-content">
			<div id="content" role="main" class="container">
			<center>
				<?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>
<?php
// The Loop
while ( have_posts() ) : the_post(); ?>

				<div class="category-recurso">
					<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('category-thumb');?></a>
					<h3 class="category-title">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
							<b> <?php the_title(); ?></b>
						</a>
					</h3>
					<?php the_excerpt(); ?>
				</div>
				<?php endwhile;  
			endif; ?>
			<?php if (  $wp_query->max_num_pages > 1 )?>
			<button class="loadmore btn btn-filled">Más <?php single_cat_title()?></button>

		</center>
			</div>
		</section>
		
	</article>
</main>

<?php get_footer(); ?>
